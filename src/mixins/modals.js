import createOrUpdateTask from "@/components/modals/createOrUpdateTask";
import createOrUpdateUser from "@/components/modals/createOrUpdateUser";

const baseModalConfig = {
	width: '600px',
	height: 'auto',
	scrollable: true
}

export default {
	methods: {
		openModalCreateOrUpdateTask(id = 0) {
			this.$modal.show(createOrUpdateTask, { id }, {
				...baseModalConfig,
				width: '650px',
			});
		},
		openModalCreateOrUpdateUser(id = 0) {
			this.$modal.show(createOrUpdateUser, { id }, {
				...baseModalConfig,
			});
		},
	},
};
