/**
 * import and init global mixins
 */

import Vue from 'vue'

import modals from '../mixins/modals'

Vue.mixin(modals)
