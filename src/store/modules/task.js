import { normalizeData } from "../../services/common";
import * as taskApi from "@/api/task";


const state = {
  tasks: {},
}

const processTasks = ({ commit }, list) => {

  let data = list.map(task => {
    let normalizeTask = { ...task }

    if(task.users.length > 0) {
      let resultUsers = normalizeData(task.users)
      commit('user/setUsers', resultUsers.list, { root: true })

      normalizeTask = {
        ...normalizeTask,
        users: resultUsers.keys
      }
    }

    return normalizeTask
  })

  let result = normalizeData(data)
  commit('setTasks', result.list)

  return result
}

const actions = {
  getTasks({commit}, data) {
    return taskApi.getTasks(data)
      .then(response => {
        let tasks = response.data.data.list
        let result = processTasks({ commit }, tasks)

        return {
          ...result,
          pagination: response.data.data.pagination
        }
      })
      .catch(error => {
        throw error;
      })
  },

  createOrUpdateTask({commit}, data) {
    return taskApi.createOrUpdateTask(data)
      .then(response => {
        let result = normalizeData([response.data.data])
        commit('setTasks', result.list)

        return result
      })
      .catch(error => {
        throw error;
      })
  },

}

const getters = {
  getTaskById: (state) => (id) => state.tasks[id] || {},
  getTasks: (state) => (list) => list.map(id => state.tasks[id] || {}),
}

const mutations = {
  setTasks(state, data) {
    state.tasks = {
      ...state.tasks,
      ...data
    }
  },
}

export default {
  namespaced: true,
  actions,
  getters,
  state,
  mutations
}
