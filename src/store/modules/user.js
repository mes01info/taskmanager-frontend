import { normalizeData } from "../../services/common";
import * as userApi from "@/api/user";


const state = {
  users: {},
}

const actions = {
  getUsers({commit}, data) {
    return userApi.getUsers(data)
      .then(response => {
        let users = response.data.data.list
        let result = normalizeData(users)
        commit('setUsers', result.list)

        return {
          ...result,
          pagination: response.data.data.pagination
        }
      })
      .catch(error => {
        throw error;
      })
  },

  createOrUpdateUser({commit}, data) {
    return userApi.createOrUpdateUser(data)
      .then(response => {
        let result = normalizeData([response.data.data])
        commit('setUsers', result.list)

        return {
          ...result,
          pagination: response.data.data.pagination
        }
      })
      .catch(error => {
        throw error;
      })
  },
}

const getters = {
  getUserById: (state) => (id) => state.users[id] || {},
  getUsers: (state) => (list) => list.map(id => state.users[id] || {}),
}

const mutations = {
  setUsers(state, data) {
    state.users = {
      ...state.users,
      ...data
    }
  },
}

export default {
  namespaced: true,
  actions,
  getters,
  state,
  mutations
}
