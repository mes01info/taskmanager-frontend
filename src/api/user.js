import axios from 'axios'

export function getUsers(data) {
  return axios.get('/user/list', { params: data })
}

export function createOrUpdateUser(data) {
  return axios.post('/user/'+(data.id > 0 ? 'update' : 'create'), data, {
    'Content-Type': 'multipart/form-data'
  })
}
