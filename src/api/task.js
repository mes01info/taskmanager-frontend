import axios from 'axios'

export function getTasks(data) {
  return axios.get('/task/list', { params: data })
}

export function createOrUpdateTask(data) {
  return axios.post('/task/'+(data.id > 0 ? 'update' : 'create'), data)
}
