export const TASK_STATUSES = {
  'wait': 'Ожидает',
  'work': 'В работе',
  'done': 'Готова',
};
