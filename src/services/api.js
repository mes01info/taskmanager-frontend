import axios from 'axios'

export function graphql(query, variables = {}) {
  //todo
  return axios.post('/graphql', {query, variables})
}

export function rest(host, params, config = {}) {
  return axios.post('/'+host, params, config)
}
