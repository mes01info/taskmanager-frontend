import Vue from 'vue'
import {setPageTitleMiddleware} from './middlewares'

import VueRouter from 'vue-router'
import TasksPage from '@/pages/Tasks.vue'
import UsersPage from '@/pages/Users.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/tasks',
  },
  {
    path: '/tasks',
    name: 'tasks',
    component: TasksPage,
    meta: { title: 'Задачи' }
  },
  {
    path: '/users',
    name: 'users',
    component: UsersPage,
    meta: { title: 'Пользователи' }
  },
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(setPageTitleMiddleware)

export default router
