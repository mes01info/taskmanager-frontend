"use strict";

import Vue from 'vue';
import axios from "axios";
import element from 'element-ui'

// Full config:  https://github.com/axios/axios#request-config
axios.defaults.baseURL = process.env.VUE_APP_ENDPOINT;
/* eslint-disable */
axios.interceptors.response.use(
  function(response) {
    if(response.data.error) {
      let error = response.data.errors
      if(error && error.length > 0) {
        element.Notification.error({
          title: 'Ошибка',
          message: 'error'
        })
      }
    }
    return response;
    },
  function(error) {
    console.error('HTTP error', error)
    throw error
  });

const _axios = axios.create({});

_axios.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
_axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    return response;
  },
  function(error) {
    // Do something with response error
    return Promise.reject(error);
  }
);

Plugin.install = function(Vue) {
  Vue.axios = _axios;
  window.axios = _axios;
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return _axios;
      }
    },
    $axios: {
      get() {
        return _axios;
      }
    },
  });
};

Vue.use(Plugin)

export default Plugin;
