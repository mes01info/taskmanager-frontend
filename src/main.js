import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'
import './plugins/axios'
import './plugins/vue-js-modal.js'
import store from './store'
import router from './router'
import './mixins'

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
